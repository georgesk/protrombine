/**
 * choix_protrombine.js
 * © 2019, Georges Khaznadar <georges.khaznadar@debian.org>
 * License: GNU GPL Version 3 or greater
 * 
 * This program modifies the properties of a set of named faces, so
 * they can be dragged around by the user. The user can also save the
 * faces' positions in a custom pattern file.
 **/

/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */

function listenForClicks() {
    document.addEventListener("click", (e) => {

	/**
	 * Make the images draggable
	 */
	function drag(tabs) {
	    if (tabs[0].title.includes("PRONOTE")){
		browser.tabs.executeScript(
		    { 
			"code": `
if(document.querySelector('#breadcrumbBandeau').innerHTML=='Trombinoscope des classes') 
{
  var movingDiv = null;
  var offset=[0,0];

  function makeDraggable(div, index){
    if (div.style.position == "absolute") return; /* pas deux fois */
    div.style.removeProperty('float');
    div.style.position = "absolute";
    var x = (index % 6)
    var y = (index - x)/6
    div.style.left=(100*x)+'px';
    div.style.top=(150*y)+'px';
    var divNom = div.children[1];
    divNom.style.width='80px';

    div.addEventListener('mousedown', function(e) {
      movingDiv = e.target.parentElement;
      movingDiv.style.zindex = 100;
      if (movingDiv.children.length !=2) movingDiv=movingDiv.parentElement;
      offset = [
        parseInt(movingDiv.style.left.replace('px','')) - e.clientX,
        parseInt(movingDiv.style.top.replace('px','')) - e.clientY
      ];
      var img = movingDiv.querySelector("img");
      img.ondragstart=function(){return false;};
      return false;
    });


  }

  function divsDraggable(t){
    var divs=t.querySelectorAll(':scope > div');
    divs.forEach(makeDraggable);

    document.addEventListener('mouseup', function() {
      movingDiv.style.zindex=1;
      movingDiv = null;
      return false;
    });

    document.addEventListener('mousemove', function(event) {
      event.preventDefault();
      if (movingDiv != null) {
        var mousePosition = {
          x : event.clientX,
          y : event.clientY
        };
        movingDiv.style.left = (mousePosition.x + offset[0]) + 'px';
        movingDiv.style.top  = (mousePosition.y + offset[1]) + 'px';
      }
      return false;
    });

  };
  var t= document.querySelectorAll(".interface_affV_client")[1];
  var ret=divsDraggable(t);
} else {
  alert('On ne peut mobiliser que les images du Trombinoscope élèves, dans PRONOTE !');
}`
		    }
		);
	    } else {
		alert("Le plugin ProTrombine n'est fait que pour Pronote");
	    }
	}

	function exporte(tabs){
	    if (tabs[0].title.includes("PRONOTE")){
		browser.tabs.executeScript(
		    { 
			"code": `
if(document.querySelector('#breadcrumbBandeau').innerHTML=='Trombinoscope des classes') 
{
  var plan="nom,left,top\\n";

  function divExport(div, index){
    var name=div.children[1].innerHTML;
    plan += name+","+div.style.left+","+div.style.top+"\\n" 
  }

  function divsExport(t){
    var nomplan="";
    var divs=t.querySelectorAll(':scope > div');
    divs.forEach(divExport);
    var p=document.createElement("p");
    p.style="width: 250px; height: 110px; position: absolute; z-index=100; top: 100px; left: 200 px; background: peachpuff; overflow: auto; padding: 10px; border: 1px navy solid; border-radius: 10px; font-size: 16px;";
    var thedata = 'data:application/csv;charset=utf-8,'+encodeURIComponent(plan);
    var a = document.createElement("a");
    a.href=thedata;
    a.style.display="none";
    a.target="_data";
    a.innerHTML="Cliquer";
    p.innerHTML="<h3>Export du plan</h3><input id='nomplan' type='text' placeholder='nom du fichier à exporter'/><br/>";
    var b=document.createElement("button");
    var export_func=function(){
      var fname=document.querySelector("#nomplan").value;
      if (fname.length==0) fname="SansNom";
      a.download=fname+".csv"
      a.click();
      setTimeout(function(){p.parentNode.removeChild(p);}, 0);
    };
    b.onclick=export_func;
    b.innerHTML="Cliquer pour exporter";
    p.appendChild(b);
    p.appendChild(a);
    t.appendChild(p);

  }

  var t= document.querySelectorAll(".interface_affV_client")[1]; 
  var ret=divsExport(t);
} else {
  alert('On ne peut gérer de plan que dans le Trombinoscope élèves, de PRONOTE !');
}
`
		    });
	    } else {
		alert("Le plugin ProTrombine n'est fait que pour Pronote");
	    }
	}
	
	function importe(tabs){
	    if (tabs[0].title.includes("PRONOTE")){
		browser.tabs.executeScript(
		    { 
			"code": `
if(document.querySelector('#breadcrumbBandeau').innerHTML=='Trombinoscope des classes') 
{
  var t= document.querySelectorAll(".interface_affV_client")[1]; 
  var movingDiv = null;
  var offset=[0,0];

  function disposePlan(places){

    var i=0; /* compteur global pour les photos non placées */

    function placement(div, index){
      var nom=div.children[1].innerHTML;
      div.style.removeProperty('float');
      div.style.position = "absolute";
      var divNom = div.children[1];
      divNom.style.width='80px';
      if (nom in places) {
        div.style.left=places[nom].left;
        div.style.top =places[nom].top;
      } else {
        /* les photos pas définie dans le plan */
        var x = i %4;
        var y = (i-x) / 4;
        div.style.left=(800+x*100)+"px";
        div.style.top =(50+150*y)+"px";
        i+=1;
      }
      /* on rend la trombine draggable */
      div.addEventListener('mousedown', function(e) {
        movingDiv = e.target.parentElement;
        movingDiv.style.zindex = 100;
        if (movingDiv.children.length !=2) movingDiv=movingDiv.parentElement;
        offset = [
          parseInt(movingDiv.style.left.replace('px','')) - e.clientX,
          parseInt(movingDiv.style.top.replace('px','')) - e.clientY
        ];
        var img = movingDiv.querySelector("img");
        img.ondragstart=function(){return false;};
        return false;
      });
    }

    var theDivs= t.querySelectorAll(":scope > div");
    theDivs.forEach(placement);

    document.addEventListener('mouseup', function() {
      movingDiv.style.zindex=1;
      movingDiv = null;
      return false;
    });

    document.addEventListener('mousemove', function(event) {
        event.preventDefault();
        if (movingDiv != null) {
          var mousePosition = {
            x : event.clientX,
            y : event.clientY
        };
        movingDiv.style.left = (mousePosition.x + offset[0]) + 'px';
        movingDiv.style.top  = (mousePosition.y + offset[1]) + 'px';
      }
      return false;
    });

  }

  var p=document.createElement("p");
  p.style="width: 360px; height: 110px; position: absolute; z-index=100; top: 100px; left: 200 px; background: peachpuff; overflow: auto; padding: 10px; border: 1px navy solid; border-radius: 10px; font-size: 16px;";
  p.innerHTML="<h3>Import du plan</h3><input id='nomplan' type='file' />";
  t.appendChild(p);
  var input=p.querySelector("#nomplan");
  input.onchange=function (e) {
    var file = e.target.files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = function(e) {
        var contents = e.target.result;
        var places = {};
        var lignes=contents.split("\\n").slice(1);
        for (var i = 0; i < lignes.length; i++){
          var nlt=lignes[i].split(",");
          places[nlt[0]]={left: nlt[1], top: nlt[2]};
        }
        disposePlan(places);
      };
      reader.readAsText(file);
    }
    setTimeout(function(){p.parentNode.removeChild(p);}, 0);
  };
} else {
  alert('On ne peut gérer de plan que dans le Trombinoscope élèves, de PRONOTE !');
}
`
		    });
	    } else {
		alert("Le plugin ProTrombine n'est fait que pour Pronote");
	    }
	}
	
	function reportError1(err){
	    alert(err.message);
	}

	/**
	 * if the event is about drag, export, import:
	 * Get the active tab,
	 * then call "drag()" or exporte() or importe()
	 */
	if (e.target.classList.contains("drag")) {
	    browser.tabs.query({active: true, currentWindow: true})
		.then(drag)
		.catch(reportError1);
	} else if (e.target.classList.contains("export")) {
	    browser.tabs.query({active: true, currentWindow: true})
		.then(exporte)
		.catch(reportError1);
	} else if (e.target.classList.contains("import")) {
	    browser.tabs.query({active: true, currentWindow: true})
		.then(importe)
		.catch(reportError1);
	} else if (e.target.classList.contains("help")) {
	    browser.tabs.create({url: "/help.html"});
	} else if (e.target.classList.contains("about")) {
	    browser.tabs.create({url: "/help.html#about"});
	}
    });
}

/**
 * When the popup loads, add a click handler.
 */
browser.tabs.executeScript({code: listenForClicks()});
