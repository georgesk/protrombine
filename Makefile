VERSION = $(shell grep '"version"' manifest.json | sed 's/.*"version": "\(.*\)".*/\1/')
ARCHIVE = ../protrombine-$(VERSION).xpi
FILES   = $(shell ls | grep -v Makefile | grep -v '~')

all: $(ARCHIVE)

$(ARCHIVE): $(FILES)
	cp help.html help.html~
	sed -i -e \
	  's%\(<dt>Version</dt><dd>\).*\(</dd>\)%\1'$(VERSION)'\2%'\
	  help.html
	@echo compression de protrombine version $(VERSION)
	rm -f $(ARCHIVE)
	zip $(ARCHIVE) $(FILES)
	mv help.html~ help.html

.PHONY: all
